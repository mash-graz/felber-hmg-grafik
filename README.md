# Vega-Lite Grafik bzgl. Kompostierung

Um die CORS-Probleme zu umgehen, sind die relevanten Dateien auch via GitLab-Pages zugänglich:

<https://mash-graz.gitlab.io/felber-hmg-grafik/>

Um im Ausdruck die beste Qualität zu erzielen, öffnet man am besten das [generiertere SVG-File](https://mash-graz.gitlab.io/felber-hmg-grafik/hmg.svg), und nutzt den Druckdialog des Browsers, um ein PDF mit der gewünschten Seitengröße zu exportieren bzw. direkt an den Drucker zu senden.

![Chart](https://mash-graz.gitlab.io/felber-hmg-grafik/hmg.svg)
